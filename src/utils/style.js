import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3, 2),
  },
  rootAppBar: {
    backgroundColor: theme.palette.background.paper,
  },
}));

export default useStyles;
