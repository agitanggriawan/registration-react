import React from 'react';
import {
  Grid,
  Paper,
  TextField,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@material-ui/core';
import moment from 'moment';
import { Formik } from 'formik';

import style from '../../utils/style';

const FormC = (props) => {
  const classes = style();
  const { formValues, handleNext, activeStep, handleBack } = props;

  return (
    <>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Formik
          enableReinitialize
          initialValues={{
            nama_wali: formValues ? formValues.nama_wali : '',
            tempat_lahir_wali: formValues ? formValues.tempat_lahir_wali : '',
            tanggal_lahir_wali: formValues
              ? moment(formValues.tanggal_lahir_wali).format('YYYY-MM-DD')
              : '',
            agama_wali: formValues ? formValues.agama_wali : '',
            pendidikan_tertinggi_wali: formValues
              ? formValues.pendidikan_tertinggi_wali
              : '',
            pekerjaan_jabatan_wali: formValues
              ? formValues.pekerjaan_jabatan_wali
              : '',
            penghasilan_wali: formValues ? formValues.penghasilan_wali : 0,
            warga_negara_wali: formValues ? formValues.warga_negara_wali : '',
            alamat_wali: formValues ? formValues.alamat_wali : '',
            telepon_rumah_wali: formValues ? formValues.telepon_rumah_wali : '',
            telepon_kantor_wali: formValues
              ? formValues.telepon_kantor_wali
              : '',
          }}
        >
          {({
            errors,
            touched,
            setTouched,
            validateForm,
            values,
            handleChange,
          }) => (
            <form noValidate>
              <Paper className={classes.root}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} md={3} lg={3}>
                    <TextField
                      autoFocus
                      margin="normal"
                      fullWidth
                      id="nama_wali"
                      label="Nama wali"
                      name="nama_wali"
                      onChange={handleChange}
                      value={values.nama_wali}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={3} lg={3}>
                    <TextField
                      margin="normal"
                      fullWidth
                      id="tempat_lahir_wali"
                      label="Tempat Lahir wali"
                      name="tempat_lahir_wali"
                      onChange={handleChange}
                      value={values.tempat_lahir_wali}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={3} lg={3}>
                    <TextField
                      margin="normal"
                      fullWidth
                      id="tanggal_lahir_wali"
                      label="Tanggal Lahir wali"
                      name="tanggal_lahir_wali"
                      onChange={handleChange}
                      value={values.tanggal_lahir_wali}
                      type="date"
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={3} lg={3}>
                    <FormControl margin="normal" fullWidth>
                      <InputLabel id="agama_wali">Agama</InputLabel>
                      <Select
                        margin="normal"
                        fullWidth
                        id="agama_wali"
                        label="agama_wali"
                        name="agama_wali"
                        onChange={handleChange}
                        value={values.agama_wali}
                      >
                        <MenuItem value="" disabled>
                          --- Pilih ---
                        </MenuItem>
                        <MenuItem value="Islam">Islam</MenuItem>
                        <MenuItem value="Kristen">Kristen</MenuItem>
                        <MenuItem value="Hindu">Hindu</MenuItem>
                        <MenuItem value="Buddha">Buddha</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} sm={12} md={4} lg={4}>
                    <TextField
                      margin="normal"
                      fullWidth
                      id="pekerjaan_jabatan_wali"
                      label="Pekerjaan / Jabatan"
                      name="pekerjaan_jabatan_wali"
                      onChange={handleChange}
                      value={values.pekerjaan_jabatan_wali}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={2} lg={2}>
                    <TextField
                      margin="normal"
                      fullWidth
                      id="penghasilan_wali"
                      label="Penghasilan Tiap Bulan"
                      name="penghasilan_wali"
                      onChange={handleChange}
                      value={values.penghasilan_wali}
                      type="number"
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={2} lg={2}>
                    <TextField
                      margin="normal"
                      fullWidth
                      id="warga_negara_wali"
                      label="Kewarganegaraan"
                      name="warga_negara_wali"
                      onChange={handleChange}
                      value={values.warga_negara_wali}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={2} lg={2}>
                    <TextField
                      margin="normal"
                      fullWidth
                      id="telepon_rumah_wali"
                      label="Telepon Rumah"
                      name="telepon_rumah_wali"
                      onChange={handleChange}
                      value={values.telepon_rumah_wali}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={2} lg={2}>
                    <TextField
                      margin="normal"
                      fullWidth
                      id="telepon_kantor_wali"
                      label="Telepon Kantor"
                      name="telepon_kantor_wali"
                      onChange={handleChange}
                      value={values.telepon_kantor_wali}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <TextField
                      margin="normal"
                      fullWidth
                      id="alamat_wali"
                      label="Alamat"
                      name="alamat_wali"
                      onChange={handleChange}
                      value={values.alamat_wali}
                      multiline
                      rows={4}
                    />
                  </Grid>
                </Grid>
              </Paper>
              <div>
                <br />
                {activeStep !== 0 && (
                  <Button
                    onClick={() => {
                      handleBack(values);
                    }}
                    className={classes.button}
                  >
                    Kembali
                  </Button>
                )}
                &emsp;
                <Button
                  className={classes.button}
                  variant="contained"
                  color="primary"
                  onClick={() =>
                    validateForm().then((errors) => {
                      if (
                        Object.entries(errors).length === 0 &&
                        errors.constructor === Object
                      ) {
                        handleNext(values);
                      } else {
                        setTouched(errors);
                      }
                    })
                  }
                >
                  Lanjut
                </Button>
              </div>
            </form>
          )}
        </Formik>
      </Grid>
    </>
  );
};

export default FormC;
